import os

from discord import Client
from dotenv import load_dotenv

from logger import Logger


class DiscordHandler(Client):
    def __init__(self, result_link, logger: Logger):
        super().__init__()

        self.result_link = result_link
        self.logger = logger

        load_dotenv()

        self.logger.info("Initialised discord client")

    async def _get_user(self):
        self.logger.info("Getting user id")
        user_id = int(os.getenv("USER_ID"))
        return await self.fetch_user(user_id)

    def run(self):
        self.logger.info("Starting discord client")
        token = os.getenv("BOT_TOKEN")
        super().run(token)

    async def on_ready(self):
        try:
            await self.send_message(self.result_link)
        except (Exception) as e:
            self.logger.error(
                "An error occured while trying to send a message to discord",
                data=self.result_link,
                exception=e,
            )
        finally:
            await self.close()

    async def send_message(self, results_link):
        user = await self._get_user()

        self.logger.info("Sending discord message")
        await user.send(results_link)