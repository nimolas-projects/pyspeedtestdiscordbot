from discord_handler import DiscordHandler
from logger import Logger
from speed_test_handler import SpeedTestHandler

logger = Logger()

speed_test_handler = SpeedTestHandler(logger)
results_link = speed_test_handler.perform_test()

discord_handler = DiscordHandler(results_link, logger)
discord_handler.run()