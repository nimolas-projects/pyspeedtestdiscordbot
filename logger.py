import json
import logging
import sys


class Logger:
    def __init__(self, is_verbose=False):
        # configuring log
        if is_verbose:
            self.log_level = logging.DEBUG
        else:
            self.log_level = logging.INFO

        log_format = logging.Formatter("[%(asctime)s] [%(levelname)s] - %(message)s")
        self.log = logging.getLogger(__name__)
        self.log.setLevel(self.log_level)

        # writing to stdout
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(self.log_level)
        handler.setFormatter(log_format)
        self.log.addHandler(handler)

    def _create_log(self, message, data=None, exception=None):
        log = {"message": message}

        if data is not None:
            log["data"] = data

        if exception is not None:
            log["exception"] = exception

        return json.dumps(log)

    def info(self, message, data=None):
        self.log.info(self._create_log(message, data))

    def error(self, message, data=None, exception=None):
        self.log.error(self._create_log(message, data, exception))