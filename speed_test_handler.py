import subprocess
from pathlib import Path

from logger import Logger


class SpeedTestHandler:
    def __init__(self, logger: Logger):
        super().__init__()

        self.logger = logger
        self.path = Path("speedtest-cli/speedtest.exe").resolve()

        self.logger.info("Ready to start speedtest")

    def parse_output(self, output: str):
        key = "Result URL:"

        for line in output.splitlines():
            if key in line:
                link = line.split(key)[1].strip()
                link += ".png"
                return link

        return "Results link not found"

    def perform_test(self):
        self.logger.info("Starting speed test")

        output = subprocess.check_output([str(self.path), "-s", "8713", "--accept-gdpr"]).decode()
        results_link = self.parse_output(output)

        self.logger.info("Speed test complete", results_link)

        return results_link